package com.example.demo.entity;

import lombok.Builder;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class User
{
    private Long id;
    private String name;
    private Role role;
    private String lastName;
    private List<Collection> collections;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Comment> comments;
}