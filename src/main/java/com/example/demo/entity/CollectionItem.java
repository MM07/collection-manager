package com.example.demo.entity;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class CollectionItem
{
    private Long id;
    private Collection collection;
    private String name;
    private List<Tag> tag;
}
