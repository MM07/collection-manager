package com.example.demo.entity;

import lombok.Builder;
import lombok.*;

import java.sql.Date;

@Data
@Builder
@NoArgsConstructor
public class Comment
{
    private Long id;
    private String message;
    private Collection collection;
    private Date createdAt;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;
}