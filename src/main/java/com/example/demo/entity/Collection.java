package com.example.demo.entity;

import lombok.*;

import java.util.List;

@Data
@Builder
public class Collection
{
    private Long id;
    private String name;
    private String description;
    private String imageurl;
    private Collection collect;
    private List<Tag> tag;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<CollectionItem> items;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Comment> comments;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private User user;
}
